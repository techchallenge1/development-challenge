import React, { useState, useEffect } from "react";
import "./App.css";
import Table from "./components/table.jsx";
import { Container, Button } from "@material-ui/core";
import formatCurrency from "./utils/formatCurrency";

import { getUsers } from "./services/users.js";
import { getApplications } from "./services/applications.js";
import { getPayments, createPayment } from "./services/payments.js";
import Pagination from '@material-ui/lab/Pagination';

const App = () => {
  /**
   * Hydrate data for the table and set state for users, applications, and payments
   */
  const [users, setUsers] = useState([]);
  const [applications, setApplications] = useState([]);
  const [payments, setPayments] = useState([]);
  const [dataLoaded, setDataLoaded] = useState(false);
  // Varibles required for sorting
  const[order,setOrder] = useState();
  const[orderBy,setOrderBy] = useState();

  useEffect(() => {
    async function fetchData() {
      const [usersData, applicationsData, paymentsData] = await Promise.all([
        getUsers(),
        getApplications(),
        getPayments(),
      ]);

      setUsers(usersData.body);
      setApplications(applicationsData.body);
      setPayments(paymentsData.body);
      setDataLoaded(true);
    }
    fetchData();
    document.getElementById("mypagination").querySelectorAll("ul")[0].style.marginLeft = "25vw";
  }, []);

  //Functions used for sorting the data with Material-UI
  function stableSort(array, comparator){
  const sta = array.map((el,index)=>[el,index]);
  sta.sort((a,b)=>{
    const order = comparator(a[0],b[0]);
    if(order!==0) return order;
    return a[1]-b[1];
  })
  return sta.map((el)=>el[0]);
}

function getComparator(order,orderBy){
  return order === 'desc' ? (a,b) => desComparator(a,b,orderBy) : (a,b)=>-desComparator(a,b,orderBy);
}

function desComparator(a,b,orderBy){
  if(b[orderBy]<a[orderBy])
  return -1;
  if(b[orderBy]>a[orderBy])
  return 1;
  return 0;
}
const handleSortRequest = id =>{
    const isAsc = orderBy === id && order ==="asc";
    setOrder(isAsc?'desc':'asc');
    setOrderBy(id);
  }
//
  const initiatePayment = async ({ applicationUuid, requestedAmount }) => {
    const { body } = await createPayment({
      applicationUuid,
      requestedAmount,
    });
    setPayments([...payments, body]);
  };

  const itemsPerPage = 10;
  const [page, setPage] = useState(1);
  const noOfPages=Math.ceil(users.length / itemsPerPage);
  const handleChange = (event, value) => {
    setPage(value);
  };
  

  //data is loaded with pagination and sorting accordingly
  let tableData = [];
  if (dataLoaded) {
    tableData = stableSort(users,getComparator(order,orderBy)).slice((page - 1) * itemsPerPage, page * itemsPerPage).map(({ uuid, name, email }) => {
      const { requestedAmount, uuid: applicationUuid } =
      applications.find((application) => application.userUuid === uuid) || {};
      const { paymentAmount, paymentMethod } =
      payments.find(
        (payment) => payment.applicationUuid === applicationUuid
      ) || {};

      // Format table data to be passed into the table component, pay button tacked
      // onto the end to allow payments to be issued for each row
      return {
        uuid,
        name,
        email,
        requestedAmount: formatCurrency(requestedAmount),
        paymentAmount: formatCurrency(paymentAmount),
        paymentMethod,
        initiatePayment: (requestedAmount!==undefined && paymentAmount===undefined) ? (
          <Button
            onClick={() =>
              initiatePayment({
                applicationUuid,
                requestedAmount,
              })
            }
            variant="contained"
          >
            Pay
          </Button>
        ) : null,
      };
    });
  }

  //Added Pagination component with Material-UI
  return (
    <div className="App">
      <Container>{dataLoaded && <Table data={tableData} order = {order} orderBy ={orderBy} fun={handleSortRequest}/>}</Container>
      <div id="mypagination"  className="clspagination">       
      <Pagination
          count={noOfPages}
          page={page}
          onChange={handleChange}
          defaultPage={1}
          showFirstButton
          showLastButton
          size = "large"
        />
  </div>

    </div>
  );
  
};

export default App;

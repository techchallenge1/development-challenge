import { render, waitFor, screen } from '@testing-library/react';
import App from './App';

test('table renders with headers', async () => {
  render(<App />);
  await waitFor(() => screen.getByRole('table'))
  const uuidHeader = screen.getByText(/Uuid/g);
  expect(uuidHeader).toBeInTheDocument();
  const nameHeader = screen.getByText(/Name/g);
  expect(nameHeader).toBeInTheDocument();
  const emailHeader = screen.getByText(/Email/g);
  expect(emailHeader).toBeInTheDocument();
  const requestedAmountHeader = screen.getByText(/Requested Amount/g);
  expect(requestedAmountHeader).toBeInTheDocument();
  const paymentAmountHeader = screen.getByText(/Payment Amount/g);
  expect(paymentAmountHeader).toBeInTheDocument();
  const paymentMethodHeader = screen.getByText(/Payment Method/g);
  expect(paymentMethodHeader).toBeInTheDocument();
  const initiatePaymentHeader = screen.getByText(/Initiate Payment/g);
  expect(initiatePaymentHeader).toBeInTheDocument();
});

//Test case to check the payment button availability
test('table check for payment', async () => {
  render(<App />);
  await waitFor(() => screen.getByRole('table'))
  const rows=screen.getAllByRole('row');
  console.log(rows[0]);
  for(var i=0;i<rows.length;i++){
    if(rows[i].cells[3]!==undefined && rows[i].cells[4]===undefined){
      expect(rows[i].cells[6].childElementCount===1)
    }
    else{
      expect(rows[i].cells[6].childElementCount===0)
    }
  }
  
});